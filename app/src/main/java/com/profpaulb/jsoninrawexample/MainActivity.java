package com.profpaulb.jsoninrawexample;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import java.io.InputStream;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void loadGrades(View view) {

        Resources res = getResources();
        InputStream is = res.openRawResource(R.raw.student_grades);
        Scanner scanner = new Scanner(is);
        StringBuilder builder = new StringBuilder();
        while (scanner.hasNextLine()) {
            builder.append(scanner.nextLine());
        }

        parseJson(builder.toString());

    }

    private void parseJson(String s) {
        TextView txtDisplay = (TextView) findViewById(R.id.text_display);
        txtDisplay.setText(s);
    }


}
